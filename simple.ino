#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <Adafruit_NeoPixel.h>
#include <Tone32.h>
#include <string.h>
#include <stdio.h>

#define LED_PIN        26
#define BUZZER_PIN 16
#define BUZZER_CHANNEL 0

#define SSID "Réseau privé de Romain"
#define PASS "#####"

#include <ArduinoJson.h>

WiFiMulti WiFiMulti;

#define NUMPIXELS 10
Adafruit_NeoPixel pixels(NUMPIXELS, LED_PIN, NEO_GRB + NEO_KHZ800);

#define DELAYVAL 500

void setup() {
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, PASS);
  Serial.print("Connecting to WiFi ...");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
  }
  Serial.println(WiFi.localIP());

  pixels.begin();
}

void loop() {
  HTTPClient http;
  
  http.begin("http://mbp-de-romain:3000/level");
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");

  String httpRequestData = "data=";

  int scales[10]{ 200, 350, 600, 750, 900, 1200, 1500, 2000, 2500, 3000 };
  int colors[10]{ pixels.Color(55, 255, 76), pixels.Color(94, 255, 55), pixels.Color(131, 255, 55), pixels.Color(179, 255, 55), pixels.Color(255, 255, 55), pixels.Color(255, 240, 55), pixels.Color(255, 213, 55), pixels.Color(255, 173, 55), pixels.Color(255, 110, 55), pixels.Color(255, 255, 255) };
  
  float value = analogRead(A2);

  float sum = 0;
  for(int i=0; i<100; i++) {
       sum = sum + analogRead(A2);
       delay(1);
  }
  sum = sum/100;
  
  httpRequestData.concat(sum);

  int httpResponseCode = http.POST(httpRequestData);

  DynamicJsonDocument doc(1024);
  String input = http.getString();
  deserializeJson(doc, input);
  JsonObject obj = doc.as<JsonObject>();

  int level = obj["level"];

  Serial.println(level);

  for(int i=0; i<10; i++) {
    if (sum >= level) {
      tone(BUZZER_PIN, NOTE_C4, 500, BUZZER_CHANNEL);
      noTone(BUZZER_PIN, BUZZER_CHANNEL);
    }
    if (sum <= scales[i]) {
      pixels.clear();
      for(int j=0; j<i; j++) {
        pixels.setPixelColor(j, colors[j]);
        pixels.setBrightness(10);
    
        pixels.show();
      }
      break;
    }
  }
  
  delay(500);
}
